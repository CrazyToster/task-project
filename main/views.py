from django.shortcuts import render_to_response, redirect
from users.forms import UserForm
from django.contrib.auth import authenticate, login

def main(request):
    return render_to_response("main.html")

def register(request):
    if request.method == 'POST': # If the form has been submitted...
        form = UserForm(request.POST) # A form bound to the POST data
        if form.is_valid():
            form.save()
            return redirect('/') # Redirect after POST
        else:
            return render_to_response("Registration/register.html", {'form' : form})
    else:
        form = UserForm()
        return render_to_response("Registration/register.html", {
        'form': form,
        })


def add_user(request):
    return render_to_response("Registration/success.html")

def login_view(request):
    user = authenticate(username = "toster", password = "gunfire")

    if user is not None:
        login(request, user)
        return render_to_response("login.html", {"message" : "valid"})
    else:
        return render_to_response("login.html", {"message" : "invalid"})