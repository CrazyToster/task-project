from django.conf.urls import patterns, include, url
from django.contrib import admin
from main.views import main
from main.views import register
from main.views import add_user
from main.views import login_view
from Tasks.views import task
from Tasks.views import new_task
from Tasks.views import new_message
from Tasks.views import edit_task
from users.views import department
from users.views import new_dep


admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$',main),
    url(r'^register$', register),
    url(r'^add_user$', add_user),
    url(r'^bundles$', task),
    url(r'new_project', new_task),
    url(r'new_message', new_message),
    url(r'department', department),
    url(r'new_dep', new_dep),
    url(r'login', login_view),
    url(r'edit_task$', edit_task),
    url(r'^admin/', include(admin.site.urls)),

)

