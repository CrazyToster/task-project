__author__ = 'Toaster'
from django.forms import ModelForm
from users.models import Department
from users.models import Worker
from django.contrib.auth.models import User

class DepartmentForm(ModelForm):
    class Meta:
        model = Department

class WorkerForm(ModelForm):
    class Meta:
        model = Worker

class UserForm(ModelForm):
    class Meta:
        model = User
        exclude = ('is_staff', 'is_active', 'last_login', 'date_joined', 'is_superuser')

