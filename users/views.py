# Create your views here.
from django.shortcuts import render_to_response
from users.models import Department
from users.forms import DepartmentForm
from django.shortcuts import redirect

def department(request):
    department = Department.objects.all()
    return render_to_response("Department/depart.html", {'departments':department})

def new_dep(request):
    if request.method == 'POST':
        form = DepartmentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/department")
        else:
            return render_to_response("Department/new_dep.html", {'form' : form})
    else:
        form = DepartmentForm()
        return render_to_response("Department/new_dep.html", {'form' : form})