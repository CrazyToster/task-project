from django.contrib.auth.models import User, Group
from django.db import models
from django.contrib import admin

class Worker(models.Model):
    ROLE = (
        (1, "Project Manager"),
        (2, "Programmer")
    )

    user = models.OneToOneField(User)
    role = models.PositiveIntegerField(choices=ROLE, default=2)

    def __str__(self):
        return self.user.username


class Department(models.Model):
    name = models.CharField(max_length=30)
    owner = models.OneToOneField(Worker)
    access = models.OneToOneField(Group)
    workers = models.ManyToManyField(Worker, blank=True,
                                     null=True, related_name='employers')
    sub_dep = models.ForeignKey("self",blank=True, null=True)

    def get_workers(self):
        worker = Worker.objects.get(id = self.id)
        return worker.workers.all()
    def __str__(self):
        return self.name

admin.site.register(Worker)
admin.site.register(Department)