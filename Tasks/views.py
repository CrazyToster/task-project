from django.shortcuts import render_to_response, redirect
from Tasks.models import Task
from Tasks.forms import ProjectForm
from Tasks.forms import MessageForm
from Tasks.forms import EditProjectForm

__id__ = 0

def task(request):
    tasks = Task.objects.all()
    return render_to_response("Task/project.html",{'tasks': tasks})

def new_task(request):
    if request.method == 'POST': # If the form has been submitted...
        form = ProjectForm(request.POST) # A form bound to the POST data
        if form.is_valid():
            form.save(commit=True)
            return redirect("/bundles") # Redirect after POST
        else:
            return render_to_response("Task/create_project.html", {'form': form,})
    else:
        form = ProjectForm()
        return render_to_response("Task/create_project.html", {
            'form': form,
            })

def new_message(request):
    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return render_to_response("Task/message.html")
        else:
            return render_to_response("Task/new_message.html", {'form': form,})
    else:
        form = MessageForm()
        return render_to_response("Task/new_message.html", {'form' : form,})

def edit_task(request):
    if 'task_id' in request.POST:
        global __id__
        __id__ = request.POST['task_id']
        curr_task = Task.objects.get(id = __id__)
        form = EditProjectForm(instance = curr_task)
        return render_to_response("Task/edit_task.html", {'form' : form},)
    else:
        form = EditProjectForm(request.POST, instance=Task.objects.get(id = __id__))
        if form.is_valid:
            form.save()
            return redirect("/bundles")
        else:
            return  render_to_response("Task/edit_task.html", {'form' : form})




