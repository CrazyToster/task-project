__author__ = 'Toaster'
from django import forms
from Tasks.models import Task
from Tasks.models import Message
from django.forms import ModelForm


class ProjectForm(ModelForm):
    class Meta:
        model = Task
        exclude = ('type')

class EditProjectForm(ModelForm):
    class Meta:
        model = Task
        exclude = ("type", "parent", "start_date", "manager")


class MessageForm(ModelForm):
    class Meta:
        model = Message