from django.db import models
from django.contrib import admin
from users.models import Worker

class Task(models.Model):

    STATUS_CHOICES = (
        (1,"Normal"),
        (2,"High"),
        (3,"Urgent"),
        (4,"Immediate"),
        (5,"Failed"),
        (6,"Succeed")
        )

    TYPE_CHOICE = (
        (1,"Project"),
        (2,"Separate task"),
        (3,"Sub task")
        )

    title = models.CharField(max_length=50)
    description = models.TextField()

    start_date =  models.DateField()
    end_date = models.DateField()

    status = models.PositiveIntegerField(choices=STATUS_CHOICES, default=1)
    type = models.PositiveIntegerField(choices=TYPE_CHOICE, default=2)

    parent = models.ForeignKey("self", blank=True,
                               null=True, related_name="root")
    performer = models.ManyToManyField(Worker, null=True,
                                       blank=True, related_name='workers')
    manager = models.ForeignKey(Worker, related_name='manager')

    def change_type(self, var):
        self.type = var

    def change_status(self, var):
        self.status = var

    def get_rel_task(self):
        return Task.objects.filter(parent = self)

    def all_rel_task(self):
        rel = []
        rel.append(self)
        for task in Task.objects.filter(parent = self):
            rel.append(task.all_rel_task)
        return rel

    def set_performer(self, _id):
        self.performer = Worker.objects.get(id=_id)

    def add_task(self, _id):
        self.tasks = Task.objects.get(id = _id)

    def get_workers(self):
        task = Task.objects.get(id = self.id)
        return task.performer.all()

    def get_message(self):
        return Message.objects.filter(rel_task = self)

    def save(self, *args, **kwargs):
        if self.parent is None:
            self.type = 2
        else:
            self.type = 3
            self.parent.type = 1
            self.parent.save()
        super(Task, self).save(*args, **kwargs)


    def __unicode__(self):
        return self.title

    class Meta:
        unique_together = ('title', 'parent')


class Message(models.Model):
    sender = models.ForeignKey(Worker, null=False, related_name='sender')
    rel_task = models.ForeignKey(Task, null=False, related_name='task')
    title = models.CharField(max_length=30)
    description = models.TextField()
    date = models.DateField()

admin.site.register(Task)
admin.site.register(Message)
